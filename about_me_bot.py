import disnake
from disnake.ext import commands

intents = disnake.Intents.default()

bot = commands.Bot(command_prefix=commands.when_mentioned, intents=intents)

@bot.event
async def on_ready():
    print(f'We have logged in as {bot.user}')

@bot.slash_command(name="aboutme", description="Tells you your Discord name and ID")
async def aboutme_slash(ctx: disnake.ApplicationCommandInteraction):
    user = ctx.author
    await ctx.send(f'Your Discord name is {user.name}#{user.discriminator} and your ID is {user.id}', ephemeral=True)

bot.run('BOT_TOKEN')