# Discord Account Info Bot

This is a simple Discord bot built using Python and the `disnake` library. The bot allows users to request their Discord name and user ID by interacting with a slash command.

## Features

- Slash command `/aboutme` that returns the user's Discord name and ID.

## Installation & Setup

1. Install the required packages:

\`\`\`bash
pip install disnake
\`\`\`

2. Create a new bot on the [Discord Developer Portal](https://discord.com/developers/applications) and invite it to your server.

3. Copy the bot token from the Discord Developer Portal and replace `BOT_TOKEN` in the code with your bot token.

4. Run the Python script:

\`\`\`bash
python bot.py
\`\`\`

## Usage

1. In a text channel on your Discord server, type `/aboutme` and press enter.

2. The bot will send an ephemeral message to you with your Discord name and ID.

## Contributing

Contributions are always welcome! If you have any ideas or suggestions, feel free to open an issue or submit a pull request. You can also reach out to the repository owner, [Ellienore Bradley](https://gitlab.com/ellienore/), for any questions or discussions related to the project.

To contribute:

1. Fork the repository.
2. Create a new branch with a descriptive name.
3. Make your changes, following the existing code style.
4. Commit your changes with clear and concise commit messages.
5. Create a merge request and explain the changes you made and why they are necessary.

Please ensure that your code is properly tested and follows the project's coding style before submitting a pull request. This will help maintain a high-quality codebase and ensure that your contributions can be easily integrated.

Thank you for your interest in contributing to the Discord Account Info Bot!
